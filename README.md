Classic Pomodoro Application. Work 25 min then get a 5 min rest. On every 4th rest, extend the rest to 15 min. Helps for increased productivity and focus. Made using python tkinter.
