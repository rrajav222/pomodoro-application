from tkinter import *
import math
# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#ffb5ba"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#fff8c7"
DARK_GREEN = "#159e00"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 15
reps = 0
timer = None
# ---------------------------- TIMER RESET ------------------------------- #


def resetTimer():
    window.after_cancel(timer)
    canvas.config(bg=YELLOW)
    canvas.itemconfig(timer_text, text="00:00")
    checkmark_label.config(text="", bg=YELLOW)
    window.config(bg=YELLOW)
    timer_label.config(text="Timer", bg=YELLOW, fg=DARK_GREEN)
    global reps
    reps = 0

# ---------------------------- TIMER MECHANISM ------------------------------- #

def beginTimer():
    if reps == 0:
        startTimer()


def startTimer():
    global reps
    reps += 1

    if reps > 8:
        reps = 0
        startTimer()

    elif reps == 1 or reps == 3 or reps == 5 or reps == 7:
        window.config(bg=GREEN)
        canvas.config(bg=GREEN)
        checkmark_label.config(bg=GREEN)
        timer_label.config(text="Work", fg=DARK_GREEN, bg=GREEN)
        countdown(WORK_MIN * 60)

    elif reps == 8:
        window.config(bg=PINK)
        canvas.config(bg=PINK)
        checkmark_label.config(bg=PINK)
        timer_label.config(text="Break", fg=RED, bg=PINK)
        countdown(LONG_BREAK_MIN * 60)

    elif reps == 2 or reps == 4 or reps == 6:
        window.config(bg=PINK)
        canvas.config(bg=PINK)
        checkmark_label.config(bg=PINK)
        timer_label.config(text="Break", fg=RED, bg=PINK)
        countdown(SHORT_BREAK_MIN * 60)


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- #


def countdown(count):
    minutes = math.floor(count / 60)
    seconds = count % 60
    if minutes < 10:
        minutes = f"0{minutes}"

    if seconds < 10:
        seconds = f"0{seconds}"

    if count > 0:
        global timer
        # below: (time,function name, positional parameters)
        timer = window.after(1000, countdown, count-1)
        canvas.itemconfig(timer_text, text=f"{minutes}:{seconds}")
        window.attributes('-topmost',False)

    else:
        startTimer()
        checkmark = ""
        work_sessions = math.floor(reps/2)
        for _ in range(work_sessions):
            checkmark += "✅"
        checkmark_label.config(text=checkmark)
        window.attributes('-topmost',True)



# ---------------------------- UI SETUP ------------------------------- #


window = Tk()
window.title("Pomodoro")
window.minsize(width=500, height=300)
window.config(padx=100, pady=100, bg=YELLOW)


# background image
canvas = Canvas(width=202, height=224, bg=YELLOW, highlightthickness=0)
tomato_img = PhotoImage(file="tomato.png")
canvas.create_image(102, 112, image=tomato_img)

# timer clock
timer_text = canvas.create_text(102, 130, text="00:00", fill="white",
                                font=(FONT_NAME, 35, "bold"))
canvas.grid(column=1, row=1)


# Timer label
timer_label = Label(text="Timer", bg=YELLOW)
timer_label.config(font=(FONT_NAME, 35, "bold"), fg=DARK_GREEN)
timer_label.grid(column=1, row=0)

# start button
button = Button(text="Start", width=10, bg="white",
                fg=DARK_GREEN, font=(FONT_NAME, 15, "bold"), command=beginTimer)
button.grid(column=0, row=3, pady=10)

# reset button
button = Button(text="Reset", width=10, bg="white",
                fg=DARK_GREEN, font=(FONT_NAME, 15, "bold"), command=resetTimer)
button.grid(column=2, row=3, pady=10)

# checkmark
checkmark_label = Label(bg=YELLOW)
checkmark_label.config(font=(FONT_NAME, 20, "bold"), fg=DARK_GREEN)
checkmark_label.grid(column=1, row=4)

window.mainloop()
